# VIM Transparent Airline theme

An installable repo for [Aiden (Ahmad) Khatib airline theme](http://www.akhatib.com/minimal-and-clean-vim-airline-theme/).

### Note

This theme is not made by me, the purpose of this repository is to allow an easy installation integrated with your favorite plugin manager.

## Installation

With [VIM-Plug](https://github.com/junegunn/vim-plug):

```
Plug 'https://gitlab.com/sunetraalex/vim-transparent-airline-theme.git'
```

then run `:PlugInstall`

and define it in your `.vimrc` / `init.vim`:

```
let g:airline_theme='transparent'
```

